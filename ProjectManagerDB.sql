USE ProductManagerDB
CREATE TABLE [dbo].[Import](
	[ImportID] [char](10) NOT NULL,
	[ImportDate] [datetime] NULL,
	[Payment] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ImportID] ASC
))
INSERT [dbo].[Import] ([ImportID], [ImportDate], [Payment]) VALUES (N'pn01      ', CAST(0x0000AC1300210840 AS DateTime), 7000)
INSERT [dbo].[Import] ([ImportID], [ImportDate], [Payment]) VALUES (N'pn02      ', CAST(0x0000AE6200210840 AS DateTime), 3000)
INSERT [dbo].[Import] ([ImportID], [ImportDate], [Payment]) VALUES (N'pn03      ', CAST(0x0000AE6200210840 AS DateTime), 30000)
INSERT [dbo].[Import] ([ImportID], [ImportDate], [Payment]) VALUES (N'pn04      ', CAST(0x0000AE6200210840 AS DateTime), 31000)
INSERT [dbo].[Import] ([ImportID], [ImportDate], [Payment]) VALUES (N'pn05      ', CAST(0x0000AC1300210840 AS DateTime), 6000)
INSERT [dbo].[Import] ([ImportID], [ImportDate], [Payment]) VALUES (N'pn06      ', CAST(0x0000AE6400000000 AS DateTime), 6000)
INSERT [dbo].[Import] ([ImportID], [ImportDate], [Payment]) VALUES (N'pn08      ', CAST(0x0000AE6400000000 AS DateTime), 16000)
INSERT [dbo].[Import] ([ImportID], [ImportDate], [Payment]) VALUES (N'prn07     ', CAST(0x0000AE6400000000 AS DateTime), 28000)

CREATE TABLE [dbo].[Account](
	[User] [char](50) NOT NULL,
	[Password] [char](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[User] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Account] ([User], [Password]) VALUES (N'admin2                                            ', N'123456                                            ')
INSERT [dbo].[Account] ([User], [Password]) VALUES (N'giang                                             ', N'123456                                            ')
INSERT [dbo].[Account] ([User], [Password]) VALUES (N'minh                                              ', N'minh1234                                          ')
INSERT [dbo].[Account] ([User], [Password]) VALUES (N'new acc                                           ', N'370722                                            ')
INSERT [dbo].[Account] ([User], [Password]) VALUES (N'nguyet                                            ', N'12345678                                          ')
INSERT [dbo].[Account] ([User], [Password]) VALUES (N'quy                                               ', N'12345678                                          ')
INSERT [dbo].[Account] ([User], [Password]) VALUES (N'test1                                             ', N'123456                                            ')
INSERT [dbo].[Account] ([User], [Password]) VALUES (N'tom                                               ', N'158111                                            ')

CREATE TABLE [dbo].[Supplier](
	[SupplierID] [char](10) NOT NULL,
	[SupplierName] [nvarchar](50) NULL,
	[PhoneNumber] [char](10) NULL,
	[Address] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[SupplierID] ASC
))
INSERT [dbo].[Supplier] ([SupplierID], [SupplierName], [PhoneNumber], [Address]) VALUES (N'Supplier01     ', N'nhà cung cấp 1', N'0235445578', N'Nam Định')
INSERT [dbo].[Supplier] ([SupplierID], [SupplierName], [PhoneNumber], [Address]) VALUES (N'Supplier02     ', N'cung cấp 2', N'0986306651', N'Ba Đình')
INSERT [dbo].[Supplier] ([SupplierID], [SupplierName], [PhoneNumber], [Address]) VALUES (N'Supplier03     ', N'Ngọc', N'0123456789', N'Nha trang')
INSERT [dbo].[Supplier] ([SupplierID], [SupplierName], [PhoneNumber], [Address]) VALUES (N'Supplier04     ', N'Huy', N'0123456789', N'Hoa Lac')
INSERT [dbo].[Supplier] ([SupplierID], [SupplierName], [PhoneNumber], [Address]) VALUES (N'Supplier05     ', N'nha cung cap 5', N'0985555741', N'Hanoi')

CREATE TABLE [dbo].[Customer](
	[CustomerID] [char](10) NOT NULL,
	[CustomerName] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[PhoneNumber] [char](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
))

INSERT [dbo].[Customer] ([CustomerID], [CustomerName], [Address], [PhoneNumber]) VALUES (N'kh01      ', N'Mai Ly', N'Cầu Giấy', N'094241234')
INSERT [dbo].[Customer] ([CustomerID], [CustomerName], [Address], [PhoneNumber]) VALUES (N'kh02      ', N'Hai Nam', N'Nam Dinh', N'014544567')
INSERT [dbo].[Customer] ([CustomerID], [CustomerName], [Address], [PhoneNumber]) VALUES (N'kh03      ', N'Khanh Manh', N'Hà Tĩnh', N'0755675432')
INSERT [dbo].[Customer] ([CustomerID], [CustomerName], [Address], [PhoneNumber]) VALUES (N'kh04      ', N'Mai', N'Nam Dinh', N'0987456123')
INSERT [dbo].[Customer] ([CustomerID], [CustomerName], [Address], [PhoneNumber]) VALUES (N'kh05      ', N'Ly', N'Tay Ho', N'0947370222')

CREATE TABLE [dbo].[Product](
	[ProductID] [char](10) NOT NULL,
	[SupplierID] [char](10) NULL,
	[ProductName] [nvarchar](50) NULL,
	[Quantity] [int] NULL,
	[UnitPrice] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
))
INSERT [dbo].[Product] ([ProductID], [SupplierID], [ProductName], [Quantity], [UnitPrice]) VALUES (N'vt01      ', N'Supplier01     ', N'sách giáo khoa', 6, 1000)
INSERT [dbo].[Product] ([ProductID], [SupplierID], [ProductName], [Quantity], [UnitPrice]) VALUES (N'vt02      ', N'Supplier02     ', N'Vật Tư 02', 4, 1000)
INSERT [dbo].[Product] ([ProductID], [SupplierID], [ProductName], [Quantity], [UnitPrice]) VALUES (N'vt03      ', N'Supplier03     ', N'Vat Tư 03', 5, 2000)
INSERT [dbo].[Product] ([ProductID], [SupplierID], [ProductName], [Quantity], [UnitPrice]) VALUES (N'vt04      ', N'Supplier04     ', N'Vật Tư 04', 8, 3000)
INSERT [dbo].[Product] ([ProductID], [SupplierID], [ProductName], [Quantity], [UnitPrice]) VALUES (N'vt05      ', N'Supplier03     ', N'bút bi', 5, 4000)
/****** Object:  Table [dbo].[Order]    Script Date: 07/05/2022 22:31:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Order](
	[OrderID] [char](10) NOT NULL,
	[DateCreate] [datetime] NULL,
	[Payment] [int] NULL,
	[CustomerID] [char](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Order] ([OrderID], [DateCreate], [Payment], [CustomerID]) VALUES (N'xk01      ', CAST(0x0000AE6300000000 AS DateTime), 3000, N'kh01      ')
INSERT [dbo].[Order] ([OrderID], [DateCreate], [Payment], [CustomerID]) VALUES (N'xk02      ', CAST(0x0000AE6300000000 AS DateTime), 6000, N'kh05      ')
INSERT [dbo].[Order] ([OrderID], [DateCreate], [Payment], [CustomerID]) VALUES (N'xk03      ', CAST(0x0000AE6300000000 AS DateTime), 3000, N'kh05      ')
INSERT [dbo].[Order] ([OrderID], [DateCreate], [Payment], [CustomerID]) VALUES (N'xk04      ', CAST(0x0000AE6300000000 AS DateTime), 3000, N'kh03      ')
INSERT [dbo].[Order] ([OrderID], [DateCreate], [Payment], [CustomerID]) VALUES (N'xk05      ', CAST(0x0000AE6300000000 AS DateTime), 6000, N'kh03      ')
INSERT [dbo].[Order] ([OrderID], [DateCreate], [Payment], [CustomerID]) VALUES (N'xk06      ', CAST(0x0000AE6300000000 AS DateTime), 8000, N'kh02      ')
/****** Object:  Table [dbo].[ImportDetail]    Script Date: 07/05/2022 22:31:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImportDetail](
	[ImportID] [char](10) NOT NULL,
	[ProductID] [char](10) NOT NULL,
	[UnitPrice] [int] NULL,
	[Quantity] [int] NULL,
	[Payment] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ImportID] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[ImportDetail] ([ImportID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'pn01      ', N'vt01      ', 1000, 7, 7000)
INSERT [dbo].[ImportDetail] ([ImportID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'pn02      ', N'vt02      ', 1000, 3, 3000)
INSERT [dbo].[ImportDetail] ([ImportID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'pn03      ', N'vt03      ', 2000, 15, 30000)
INSERT [dbo].[ImportDetail] ([ImportID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'pn04      ', N'vt04      ', 3000, 5, 15000)
INSERT [dbo].[ImportDetail] ([ImportID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'pn05      ', N'vt01      ', 1000, 6, 6000)
INSERT [dbo].[ImportDetail] ([ImportID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'pn06      ', N'vt02      ', 1000, 4, 4000)
INSERT [dbo].[ImportDetail] ([ImportID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'pn08      ', N'vt04      ', 4000, 4, 16000)
INSERT [dbo].[ImportDetail] ([ImportID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'prn07     ', N'vt05      ', 4000, 7, 28000)
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 07/05/2022 22:31:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[OrderID] [char](10) NOT NULL,
	[ProductID] [char](10) NOT NULL,
	[UnitPrice] [int] NULL,
	[Quantity] [int] NULL,
	[Payment] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'hd01      ', N'vt01      ', 1000, 10, 10000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'hd02      ', N'vt02      ', 1000, 2, 2000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'hd03      ', N'vt03      ', 2000, 10, 20000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'hd04      ', N'vt04      ', 3000, 5, 15000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'hd05      ', N'vt01      ', 1000, 3, 3000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'hd05      ', N'vt04      ', 3000, 4, 12000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'xk01      ', N'vt01      ', 1000, 3, 3000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'xk02      ', N'vt04      ', 3000, 2, 6000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'xk03      ', N'vt02      ', 1000, 3, 3000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'xk04      ', N'vt01      ', 1000, 3, 3000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'xk05      ', N'vt01      ', 1000, 6, 6000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Payment]) VALUES (N'xk06      ', N'vt05      ', 4000, 2, 8000)

ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([SupplierID])
REFERENCES [dbo].[Supplier] ([SupplierID])
GO


/****** Object:  ForeignKey [FK__Order__CustomerID__33D4B598]    Script Date: 07/05/2022 22:31:44 ******/
ALTER TABLE [dbo].[Order]  WITH CHECK ADD FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD FOREIGN KEY([OrderID])
REFERENCES [dbo].[Order] ([OrderID])
GO

/****** Object:  ForeignKey [FK__CTPhieuNha__ImportID__31EC6D26]    Script Date: 07/05/2022 22:31:44 ******/
ALTER TABLE [dbo].[ImportDetail]  WITH CHECK ADD FOREIGN KEY([ImportID])
REFERENCES [dbo].[Import] ([ImportID])
GO

/****** Object:  ForeignKey [FK__CTPhieuNha__ProductID__32E0915F]    Script Date: 07/05/2022 22:31:44 ******/
ALTER TABLE [dbo].[ImportDetail]  WITH CHECK ADD FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
